[Lopolis](https://lopolis.si/) does not support sending e-mail
notifications for all messages you receive there. Code here
checks for new messages and fails a CI job if a new message is found.
