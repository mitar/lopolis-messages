#!/bin/bash

touch messages.txt || exit 10

# We have to fetch latest job ID ourselves, because we want
# to fetch the artifact of the latest failed job as well.
# See: https://gitlab.com/gitlab-org/gitlab/-/issues/368626
last_job_id=`curl --fail --location --header "PRIVATE-TOKEN: $API_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs?scope[]=failed&scope[]=success" |  jq '.[0].id'`
curl --fail --location --output messages.txt --header "PRIVATE-TOKEN: $API_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/$last_job_id/artifacts/messages.txt?job=run" || exit 10

./lopolis.py > messages-new.txt || exit 10
diff messages.txt messages-new.txt
code="$?"
mv messages-new.txt messages.txt || exit 10
exit "$code"
