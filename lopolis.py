#!/usr/bin/env python3
#
# This program outputs IDs of current messages on Lopolis platform for given user.
# It expects LOPOLIS_EMAIL and LOPOLIS_PASSWORD environment variables.

import os
import urllib.parse

import pyquery
import requests

s = requests.Session()

r = s.get('https://www.lopolis.si/')
r.raise_for_status()
request_verification_token = pyquery.PyQuery(r.text)("input[name='__RequestVerificationToken']").attr("value")

r = s.post('https://www.lopolis.si/Account/SignIn', data={
  '__RequestVerificationToken': request_verification_token,
  'LoginViewModel.UiId': '',
  'LoginViewModel.Email':	os.environ['LOPOLIS_EMAIL'],
  'LoginViewModel.Password': 	os.environ['LOPOLIS_PASSWORD'],
})
r.raise_for_status()

fragment = urllib.parse.urlparse(r.url).fragment
query = urllib.parse.urlparse(fragment.replace("/&", "/?", 1)).query
access_token = urllib.parse.parse_qs(query)['access_token'][0]

r = s.get('https://portal.lopolis.si/api/Message/FindMessages/?getCatalogues=true&messageTypeId=1&personStudentId=0', headers={
  'Authorization': 'Bearer ' + access_token,
})
r.raise_for_status()
for message in r.json()['messageDTOs']:
  print(message['messageId'])
